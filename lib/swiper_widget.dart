import 'package:flutter/material.dart';

class SwiperWidget extends StatefulWidget {
  SwiperWidget({required this.list, this.containerHeight = 180});

  final List<String> list;
  final double containerHeight;
  @override
  _SwiperWidgetState createState() => _SwiperWidgetState();
}

class _SwiperWidgetState extends State<SwiperWidget> {
  int _selectedPage = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: widget.containerHeight,
          width: MediaQuery.of(context).size.width,
          child: PageView.builder(
            onPageChanged: (value) {
              setState(() {
                _selectedPage = value;
              });
            },
            itemCount: widget.list.length,
            itemBuilder: (context, index) => Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(36),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: Offset(0, 2),
                    ),
                  ],
                  image: DecorationImage(
                    image: AssetImage(widget.list[index]),
                    fit: BoxFit.cover,
                  ),
                ),
                child: null),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
              widget.list.length, (index) => buildDot(index: index)),
        ),
      ],
    );
  }

  AnimatedContainer buildDot({required int index}) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: _selectedPage == index ? 20 : 6,
      decoration: BoxDecoration(
          color: _selectedPage == index ? Color(0xFF383838) : Color(0xFFC6C6C6),
          borderRadius: BorderRadius.circular(3)),
    );
  }
}
