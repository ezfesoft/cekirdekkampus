import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:line_icons/line_icon.dart';
import 'package:url_launcher/url_launcher.dart';

class FooterMobile extends StatefulWidget {
  const FooterMobile({Key? key}) : super(key: key);

  @override
  State<FooterMobile> createState() => _FooterMobileState();
}

class _FooterMobileState extends State<FooterMobile> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.width > 1000 ? 250 : 450,
        color: HexColor("#fbcb65"),
        child: MediaQuery.of(context).size.width > 1000
            ? Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width * .3,
                      //color: Colors.green,
                      child: Column(
                        children: [
                          AutoSizeText(
                            'Hakkımızda',
                            maxLines: 1,
                            presetFontSizes: [20, 18, 15],
                            textAlign: TextAlign.left,
                            style: GoogleFonts.montserrat(
                              textStyle: Theme.of(context).textTheme.headline2,
                              color: HexColor("#383841"),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Divider(
                            height: 10,
                            //color:HexColor("#"),
                            color: Colors.brown,
                            indent: 20,
                            endIndent: 20,
                          ),
                          AutoSizeText(
                            'İlköğretim 4. sınıftan 12. sınıfa kadar Türkiye’nin 81 ilinde evimdekiegitim.com, öğrencilerinin evlerinden derslere katılarak yollarda zaman kaybetmelerini engelliyor, trafik sorunu ve riski ile karşılaşmadan eğitim almalarını sağlıyor. evimdekiegitim.com her seviyede, okullarda uygulanan müfredat programını sunmasının yanında öğrencilerini uzman öğretmen kadrosu ile sınavlarına hazırlıyor.',
                            maxLines: 10,
                            presetFontSizes: [14, 11, 9],
                            textAlign: TextAlign.left,
                            style: GoogleFonts.montserrat(
                              textStyle: Theme.of(context).textTheme.headline2,
                              color: HexColor("#383841"),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //---------------------
                    Container(
                      width: MediaQuery.of(context).size.width * .3,
                      padding: EdgeInsets.all(20),
                      //color: Colors.green,
                      child: Column(
                        children: [
                          AutoSizeText(
                            'İletişim',
                            maxLines: 1,
                            presetFontSizes: [20, 18, 15],
                            textAlign: TextAlign.center,
                            style: GoogleFonts.montserrat(
                              textStyle: Theme.of(context).textTheme.headline2,
                              color: HexColor("#383841"),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Divider(
                            height: 10,
                            //color:HexColor("#"),
                            color: Colors.brown,
                            indent: 20,
                            endIndent: 20,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          GestureDetector(
                              onTap: () {
                                launch("https://g.page/oxo-robotic?share");
                              },
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.home,
                                    color: Colors.brown,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    width: 225,
                                    child: AutoSizeText(
                                      'Yaka Mah. Şehit Mehmet Uysal Sok No 11 Meram Konya',
                                      maxLines: 2,
                                      presetFontSizes: [14, 11, 9],
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.montserrat(
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .headline2,
                                        color: HexColor("#383841"),
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    //color: Colors.green,
                                  )
                                ],
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.mail,
                                color: Colors.brown,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              AutoSizeText(
                                'cekirdekkampus@gmail.com',
                                maxLines: 10,
                                presetFontSizes: [15, 12, 9],
                                textAlign: TextAlign.center,
                                style: GoogleFonts.montserrat(
                                  textStyle:
                                      Theme.of(context).textTheme.headline2,
                                  color: HexColor("#383841"),
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          GestureDetector(
                              onTap: () {
                                launch("tel://03323241175");
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.phone,
                                    color: Colors.brown,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  AutoSizeText(
                                    '+90 (332) 324 11 75',
                                    maxLines: 10,
                                    presetFontSizes: [15, 12, 9],
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.montserrat(
                                      textStyle:
                                          Theme.of(context).textTheme.headline2,
                                      color: HexColor("#383841"),
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          GestureDetector(
                              onTap: () {
                                launch(
                                    "https://api.whatsapp.com/send/?phone=903323241175");
                              },
                              child: Row(
                                children: [
                                  LineIcon.whatSApp(
                                    color: Colors.brown,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * .7,
                                    child: AutoSizeText(
                                      'Whatsapp Üzerinden İletişime Geçmek İçin Tıklayın',
                                      maxLines: 2,
                                      presetFontSizes: [12, 10, 8],
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.montserrat(
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .headline2,
                                        color: HexColor("#383841"),
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  )
                                ],
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              )
            : Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 100, right: 100),
                      //width: MediaQuery.of(context).size.width * .3,
                      //color: Colors.green,
                      child: Column(
                        children: [
                          AutoSizeText(
                            'Hakkımızda',
                            maxLines: 1,
                            presetFontSizes: [20, 18, 15],
                            textAlign: TextAlign.left,
                            style: GoogleFonts.montserrat(
                              textStyle: Theme.of(context).textTheme.headline2,
                              color: HexColor("#383841"),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Divider(
                            height: 10,
                            //color:HexColor("#"),
                            color: Colors.brown,
                            indent: 20,
                            endIndent: 20,
                          ),
                          AutoSizeText(
                            'İlköğretim 4. sınıftan 12. sınıfa kadar Türkiye’nin 81 ilinde Çekirdek Kampüs, öğrencilerinin evlerinden derslere katılarak yollarda zaman kaybetmelerini engelliyor, trafik sorunu ve riski ile karşılaşmadan eğitim almalarını sağlıyor. evimdekiegitim.com her seviyede, okullarda uygulanan müfredat programını sunmasının yanında öğrencilerini uzman öğretmen kadrosu ile sınavlarına hazırlıyor.',
                            maxLines: 8,
                            presetFontSizes: [15, 12, 9],
                            textAlign: TextAlign.left,
                            style: GoogleFonts.montserrat(
                              textStyle: Theme.of(context).textTheme.headline2,
                              color: HexColor("#383841"),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    //---------------------
                    Container(
                      // width: MediaQuery.of(context).size.width * .3,
                      padding: EdgeInsets.only(left: 100, right: 100),
                      //color: Colors.green,
                      child: Column(
                        children: [
                          AutoSizeText(
                            'İletişim',
                            maxLines: 1,
                            presetFontSizes: [20, 18, 15],
                            textAlign: TextAlign.center,
                            style: GoogleFonts.montserrat(
                              textStyle: Theme.of(context).textTheme.headline2,
                              color: HexColor("#383841"),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Divider(
                            height: 10,
                            //color:HexColor("#"),
                            color: Colors.brown,
                            indent: 20,
                            endIndent: 20,
                          ),
                          GestureDetector(
                              onTap: () {
                                launch("https://g.page/oxo-robotic?share");
                              },
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.home,
                                    color: Colors.brown,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * .5,
                                    child: AutoSizeText(
                                      'Yaka Mah. Şehit Mehmet Uysal Sok No 11 Meram Konya',
                                      maxLines: 2,
                                      presetFontSizes: [14, 11, 9],
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.montserrat(
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .headline2,
                                        color: HexColor("#383841"),
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    //color: Colors.green,
                                  )
                                ],
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.mail,
                                color: Colors.brown,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              AutoSizeText(
                                'cekirdekkampus@gmail.com',
                                maxLines: 10,
                                presetFontSizes: [15, 12, 9],
                                textAlign: TextAlign.center,
                                style: GoogleFonts.montserrat(
                                  textStyle:
                                      Theme.of(context).textTheme.headline2,
                                  color: HexColor("#383841"),
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          GestureDetector(
                              onTap: () {
                                launch("tel://03323241175");
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.phone,
                                    color: Colors.brown,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  AutoSizeText(
                                    '+90 (332) 324 11 75',
                                    maxLines: 10,
                                    presetFontSizes: [15, 12, 9],
                                    textAlign: TextAlign.left,
                                    style: GoogleFonts.montserrat(
                                      textStyle:
                                          Theme.of(context).textTheme.headline2,
                                      color: HexColor("#383841"),
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          GestureDetector(
                              onTap: () {
                                launch(
                                    "https://api.whatsapp.com/send/?phone=903323241175");
                              },
                              child: Row(
                                children: [
                                  LineIcon.whatSApp(
                                    color: Colors.brown,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * .5,
                                    child: AutoSizeText(
                                      'Whatsapp Üzerinden İletişime Geçmek İçin Tıklayın',
                                      maxLines: 2,
                                      presetFontSizes: [12, 10, 8],
                                      textAlign: TextAlign.left,
                                      style: GoogleFonts.montserrat(
                                        textStyle: Theme.of(context)
                                            .textTheme
                                            .headline2,
                                        color: HexColor("#383841"),
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  )
                                ],
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ));
  }
}
