import 'package:auto_size_text/auto_size_text.dart';
import 'package:cekirdek_web/swiper_widget.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class BannerWidget extends StatefulWidget {
  const BannerWidget({Key? key}) : super(key: key);

  @override
  State<BannerWidget> createState() => _BannerWidgetState();
}

class _BannerWidgetState extends State<BannerWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 450,
        color: Colors.white,
        child: Stack(
          children: [
            SwiperWidget(
              containerHeight: 400,
              list: [
                "assets/images/banner1.jpeg",
                "assets/images/banner4.jpeg",
                "assets/images/silde-2.jpeg",
                "assets/images/slide-4.jpeg",
                "assets/images/slide-5.jpeg",
              ],
            ),
            Container(
              height: 380,
              margin: EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 15,
              ),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: FractionalOffset.topCenter,
                    end: FractionalOffset.bottomCenter,
                    colors: [
                      HexColor("#191919").withOpacity(0.2),
                      HexColor("#191919").withOpacity(0.99),
                    ],
                  ),
                  borderRadius: BorderRadius.circular(36),
                  color: HexColor("##000000")),
              child: Center(
                  child: AutoSizeText(
                'Kampanyalar',
                maxLines: 1,
                presetFontSizes: [25, 20, 15, 10],
                textAlign: TextAlign.center,
                style: GoogleFonts.montserrat(
                  textStyle: Theme.of(context).textTheme.headline4,
                  color: HexColor("#f4f3f2"),
                  fontWeight: FontWeight.w400,
                ),
              )),
            ),
          ],
        ));
  }
}
