import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class Info1Mobile extends StatefulWidget {
  const Info1Mobile({Key? key}) : super(key: key);

  @override
  State<Info1Mobile> createState() => _Info1MobileState();
}

class _Info1MobileState extends State<Info1Mobile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.width > 1000 ? 850 : 500,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 20,
          ),
          AutoSizeText(
            'Bizi Neden Seçmelisiniz?',
            maxLines: 1,
            presetFontSizes: [25, 20, 15],
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
              textStyle: Theme.of(context).textTheme.headline2,
              color: HexColor("#383841"),
              fontWeight: FontWeight.w400,
            ),
          ),
          Divider(
            color: HexColor("#e19362"),
            endIndent: MediaQuery.of(context).size.width * 0.45,
            indent: MediaQuery.of(context).size.width * 0.45,
            height: 10,
          ),
          AutoSizeText(
            'Çekirdek Kampüsün Faydaları',
            maxLines: 1,
            presetFontSizes: [15, 10, 8],
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
              color: HexColor("#383841").withOpacity(.5),
              fontWeight: FontWeight.w400,
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(Icons.wifi_outlined),
              SizedBox(
                width: 20,
              ),
              Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText(
                        'Uzman Eğitmen Kadrosu',
                        maxLines: 1,
                        presetFontSizes: [20, 15, 10],
                        textAlign: TextAlign.left,
                        style: GoogleFonts.montserrat(
                          color: HexColor("#383841").withOpacity(.9),
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      AutoSizeText(
                        'Alanında uzman, deneyimli, teknoloji ve yeniliklere açık eğitmen kadromuzun amacı, öğrencilerimizin herhangi bir derse önyargılı yaklaşmamaları ve öğrenmekten korkmamaları için dersleri daima keyifli hale getirebilmektir. Bu yüzden bilim ve teknolojinin sunduğu imkanlardan yararlanarak ve öğrencilere yanlarında olduklarını hissettirerek üst düzey eğitim vermeyi ilke edinmişlerdir.',
                        maxLines: 5,
                        presetFontSizes: [16, 13, 10],
                        textAlign: TextAlign.left,
                        style: GoogleFonts.montserrat(
                          color: HexColor("#383841").withOpacity(.6),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  )),
              /*
              Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.grey)),
                child: Image.asset(
                  "assets/images/info1-1.jpg",
                  height: MediaQuery.of(context).size.width > 1000 ? 150 : 100,
                  fit: BoxFit.cover,
                ),
              )*/
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(Icons.signal_wifi_off_outlined),
              SizedBox(
                width: 20,
              ),
              Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText(
                        'Güncel Müfredat',
                        maxLines: 1,
                        presetFontSizes: [20, 15, 10],
                        textAlign: TextAlign.left,
                        style: GoogleFonts.montserrat(
                          color: HexColor("#383841").withOpacity(.9),
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      AutoSizeText(
                        'Bütün derslerimiz MEB müfredatına uygun ve paralel olarak işlenmektedir. Ders yılı başında planlarımız belirlenir ve öğrencilerimiz ders programlarını ve konuları günlük olarak ‘Takvim’ bölümünden takip ederler. Öğrencilerimizin okul derslerine hazır olabilmeleri için, konu yoğunluğuna bağlı olarak programlarımız genellikle 1 hafta önden gitmektedir.',
                        maxLines: 5,
                        presetFontSizes: [16, 13, 10],
                        textAlign: TextAlign.left,
                        style: GoogleFonts.montserrat(
                          color: HexColor("#383841").withOpacity(.6),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  )), /*
              Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.grey)),
                child: Image.asset(
                  "assets/images/info1-2.jpg",
                  height: MediaQuery.of(context).size.width > 1000 ? 150 : 100,
                  fit: BoxFit.cover,
                ),
              )*/
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(Icons.menu_book),
              SizedBox(
                width: 20,
              ),
              Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText(
                        'Zamandan Tasarruf',
                        maxLines: 1,
                        presetFontSizes: [20, 15, 10],
                        textAlign: TextAlign.left,
                        style: GoogleFonts.montserrat(
                          color: HexColor("#383841").withOpacity(.9),
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      AutoSizeText(
                        'Evimdeki Eğitim olarak amacımız öğrencilerimizin yoğun okul tempolarından uzaklaşıp, evlerinin rahat ve güvenli ortamında zamandan tasarruf ederek kaliteli eğitime kolay ulaşmalarını sağlamaktır. Bizim için önemli olan öğrencilerimiz ders çalışmak istediklerinde zaman ve mekan kısıtlaması olmaması, ihtiyaçları duydukları anda onlara iyi eğitimi sunabilmektir.',
                        maxLines: 5,
                        presetFontSizes: [16, 13, 10],
                        textAlign: TextAlign.left,
                        style: GoogleFonts.montserrat(
                          color: HexColor("#383841").withOpacity(.6),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  )), /*
              Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.grey)),
                child: Image.asset(
                  "assets/images/info1-1.jpg",
                  height: MediaQuery.of(context).size.width > 1000 ? 150 : 100,
                  fit: BoxFit.cover,
                ),
              )*/
            ],
          ),
        ],
      ),
    );
  }
}
