import 'package:auto_size_text/auto_size_text.dart';
import 'package:cekirdek_web/profile/app/features/dashboard/views/screens/dashboard_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:line_icons/line_icon.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:url_launcher/url_launcher.dart';

class HeaderMobileWidget extends StatefulWidget {
  const HeaderMobileWidget({Key? key}) : super(key: key);

  @override
  State<HeaderMobileWidget> createState() => _HeaderMobileWidgetState();
}

class _HeaderMobileWidgetState extends State<HeaderMobileWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 175,
        color: Colors.white,
        child: Stack(
          children: [
            /*
            Transform.translate(
                offset: Offset(0, 0),
                child: Container(
                  height: 50,
                  //color: Colors.red,
                  child: Row(
                    children: [
                      FlatButton(
                        onPressed: () {
                          launch("https://www.facebook.com/cekirdek_kampus/");
                        },
                        child: LineIcon.facebook(),
                      ),
                      FlatButton(
                          onPressed: () {
                            launch(
                                "https://www.instagram.com/cekirdek_kampus/");
                          },
                          child: LineIcon.instagram()),
                      FlatButton(
                          onPressed: () {
                            launch("https://www.twitter.com/cekirdek_kampus/");
                          },
                          child: LineIcon.twitterSquare()),
                      FlatButton(
                          onPressed: () {
                            launch("https://www.youtube.com/cekirdek_kampus/");
                          },
                          child: LineIcon.youtube()),
                      Spacer(),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('/dashboard');
                          Get.lazyPut(() => DashboardController());
                        },
                        child: Container(
                            alignment: Alignment.center,
                            height: 50,
                            child: Row(
                              children: [
                                AutoSizeText(
                                  'Giriş Yap',
                                  maxLines: 1,
                                  presetFontSizes: [16, 15, 10],
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.montserrat(
                                    textStyle:
                                        Theme.of(context).textTheme.headline4,
                                    color: HexColor("#383841"),
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                LineIcon.user()
                              ],
                            )),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                    ],
                  ),
                )),
                */

            Transform.translate(
                offset: Offset(MediaQuery.of(context).size.width - 150, 0),
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/dashboard');
                    Get.lazyPut(() => DashboardController());
                  },
                  child: Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: 100,
                      child: Row(
                        children: [
                          Spacer(),
                          AutoSizeText('Giriş Yap',
                              maxLines: 2,
                              presetFontSizes: [10, 9, 8, 7],
                              textAlign: TextAlign.center,
                              style: GoogleFonts.montserrat(
                                color: HexColor("#383841"),
                              )),
                          LineIcon.user()
                        ],
                      )),
                )),
            SizedBox(
              width: 10,
            ),
            Transform.translate(
                offset: Offset(0, 50),
                child: Container(
                  height: 100,
                  color: HexColor("#fbcb65"),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 20,
                      ),
                      Flexible(
                          child: FlatButton(
                        onPressed: () {},
                        child: Container(
                          alignment: Alignment.centerLeft,
                          height: 150,
                          width: MediaQuery.of(context).size.width * .3,
                          child: Icon(
                            Icons.menu_outlined,
                          ),
                        ),
                      )),
                      Container(
                        width: 200,
                        height: 200,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    Image.asset(
                      "assets/icons/cekirdek_logo1.png",
                      width: 75,
                      height: 75,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Çekirdek\nKampüs',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                        textStyle: Theme.of(context).textTheme.headline4,
                        fontSize: 25,
                        color: HexColor("#383841"),
                        fontWeight: FontWeight.w700,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  width: 20,
                ),
              ],
            ),
            Transform.translate(
              offset: Offset(0, 145),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: () {
                        launch("https://www.facebook.com/cekirdek_kampus/");
                      },
                      child: Container(
                          height: 40,
                          width: 50,
                          //color: Colors.green,
                          child: LineIcon.facebook())),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                      onTap: () {
                        launch("https://www.instagram.com/cekirdek_kampus/");
                      },
                      child: Container(
                          height: 40, width: 50, child: LineIcon.instagram())),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                      onTap: () {
                        launch("https://www.twitter.com/cekirdek_kampus/");
                      },
                      child: Container(
                          height: 40,
                          width: 50,
                          child: LineIcon.twitterSquare())),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                      onTap: () {
                        launch("https://www.youtube.com/cekirdek_kampus/");
                      },
                      child: Container(
                          height: 40, width: 50, child: LineIcon.youtube())),
                ],
              ),
            )
          ],
        ));
  }
}
